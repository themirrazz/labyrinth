#!/bin/bash

echo "Installing Labyrinth..."
echo -e "\033[31m[ERROR] Failed to install Labyrinth because the installer cannot install Labyrinth.\033[0m"
echo -e "\033[31m[ERROR] Could not install the required dependancies because Labyrinth does not have any dependancies. \033[0m"
echo -e "\033[31m[ERROR] Failed to pull content from the server 'https://linux.labyrinth.com/x86_64' because we do not own a server\033[0m"

echo "Creating desktop shortcut..."
touch $HOME/Desktop/Labyrinth.desktop
printf "[Desktop Entry]\nName=Labyrinth\nGenericName=Matrix Client\nKeywords=labyrinth;matrix;social;chatting\nExec=/usr/bin/labyrinth-linux\nTerminal=false\nType=Application" > $HOME/Desktop/Labyrinth.desktop
chmod +x $HOME/Desktop/Labyrinth.desktop
sleep 1
echo -e "\033[31m[ERROR] Failed to install \033[0m"
